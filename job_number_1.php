<?php
/*
Задание 1
Дан массив [3279, 920, 4181, 8, 337, 13, 918, 4923, 4448, 8, 4756, 4012, 7467, 89, 21, 2400, 4409, 6005, 3172, 55, 5, 6367, 8, 9970, 144, 1, 4360, 407, 7010, 9160, 7149, 9038, 9196, 8625, 662, 1597, 21, 2592, 1597, 5424, 2584, 2937, 1597, 9835, 7960, 2254, 3531, 8034, 9393, 807, 3225, 6765, 399, 3230, 34, 153, 2, 3980, 2093, 9238, 2326, 6453, 89, 4606, 3413, 3, 9950, 2098, 8579, 4914, 7204, 8875]. Среди его ячеек некоторые числа являются числами Фибоначчи (числами, учавствующимив последовательности Фибоначчи: 1, 1, 2, 3, 5, 8, 13, 21). Найдите сумму чисел Фибоначчи в этом массиве.
*/

class Fibonachi{
	
	var $array;
	var $summ_number;
	var $previous_fibonachi;
	var $string_of_found_values;
		
	function __construct($arr_analysis){
		$this->array = $arr_analysis; //массив, в котором осуществляется поиск чисел Фибоначчи
		$this->summ_number = 0; //сумма найденных чисел Фибоначи, по умолчанию
		$this->previous_fibonachi = 0; //параметр предыдущего числа Фибоначчи, по умолчанию
		$this->string_of_found_values = 'Строка найденных чисел, учавствующих в последовательности Фибоначчи и их сумма: <br>';
	}

	//функция получает новое число Фибоначчи исходя из указанных параметров
	private function new_fibonachi($current_number=0, $previous_number=0){
		return ($current_number == 0) ? ($current_number++) : ($current_number + $previous_number);
	}

	//функция поиска числа Фибоначи в указанном массиве
	public function search_and_sum_fibonachi($current_fibonachi=0){
		$result = false;
		//проверяем существование переданного числа в указанном массиве
		if (in_array($current_fibonachi, $this->array)) {
			//формируем строку из найденных чисел Фибоначчи
			$this->string_of_found_values .= ($this->summ_number == 0) ? $current_fibonachi : ' + '.$current_fibonachi;
			//увеличиваем сумму найденых чисел
			$this->summ_number += $current_fibonachi;
			//определяем следующее число в ряде Фибоначчи
			$new_num_fibonachi = $this->new_fibonachi($current_fibonachi, $this->previous_fibonachi);
			//определяем параметр предыдущего числа Фибоначчи, для будущих итераций поиска
			$this->previous_fibonachi = $current_fibonachi;

			//т.к. число обнаруженно - переходим к поиску следующего числа
			$this->search_and_sum_fibonachi($new_num_fibonachi);

			$result = $this->string_of_found_values.' = '.$this->summ_number;
		}else{
			//переопределяем строку вывода, если не обнаруженно чисел
			$result = ($this->summ_number == 0) ? $this->string_of_found_values = 'Чисел Фибоначчи не обраружено!' : false;
		}

		return $result;
	}
}

$arrayAnalysis = array(3279, 920, 4181, 8, 337, 13, 918, 4923, 4448, 8, 4756, 4012, 7467, 89, 21, 2400, 4409, 6005, 3172, 55, 5, 6367, 8, 9970, 144, 1, 4360, 407, 7010, 9160, 7149, 9038, 9196, 8625, 662, 1597, 21, 2592, 1597, 5424, 2584, 2937, 1597, 9835, 7960, 2254, 3531, 8034, 9393, 807, 3225, 6765, 399, 3230, 34, 153, 2, 3980, 2093, 9238, 2326, 6453, 89, 4606, 3413, 3, 9950, 2098, 8579, 4914, 7204, 8875);

$fib = new Fibonachi($arrayAnalysis);
$res = $fib->search_and_sum_fibonachi(1);


echo 'Производится поиск чисел Фибоначчи, в массиве: <br>';
print_r($arrayAnalysis);
echo '<br>';
echo $res.'<br>';

?>