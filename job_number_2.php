<?php
/*
Задание 2
Возьмите картинку https://alef.im/php-test-colors.jpg, без использования сторонних библиотек анализа изображений, найдите самый распространенный цвет на этом изображении. Выведите его в формате #FFFFFF

////////////////////////////////

В данном задании я не смог обойтись без библиотек пришлось воспользоваться библиотекой GD.
Иначе, просто не смог отыскать решения, надеюсь в вашем задании GD не считается сторонней библиотекой.
Саму обработку изображения в функции get_color_parts_img() я писал, руководствуясь разными статьями из сети.
Остальные функции и сам класс - моего производства.
*/

class getMainColorInImage{	
	var $array_colors;
	var $url_img;
	var $parts_size;
	var $result_color;
		
	function __construct($url_img){
		$this->array_colors; //массив, цветов для сортировки
		$this->url_img = $url_img; //путь к файлу картинку
		$this->parts_size = array(20, 20); //массив областей на которые будет разбиваться исходное изображение
		$this->result_color; //конечный цвет
	}

	//функция - получение массива с цветами частей изображения
	private function get_color_parts_img(){ 
		//проверка ввода значений
	    if($this->url_img) {    
			//создаем область изображения jpg
			$img = imagecreatefromjpeg($this->url_img);
			//Получение размера изображения
			$img_sizes=getimagesize($this->url_img);
			//Создание нового полноцветного изображения
			$resized_img=imagecreatetruecolor($this->parts_size[0], $this->parts_size[1]);
			//Копирование и изменение размера части изображения
			imagecopyresized($resized_img, $img , 0, 0 , 0, 0, $this->parts_size[0], $this->parts_size[1], $img_sizes[0], $img_sizes[1]);
			//Уничтожение изображения
			imagedestroy($img);

			//массив для набора цветов
			$colors = array();
			for($i = 0; $i < $this->parts_size[1]; $i++) {
			    for($j=0; $j < $this->parts_size[0]; $j++) {
			    	//Получение индекса цвета пиксела
			    	array_push($colors, dechex(imagecolorat($resized_img,$j,$i)));
				}
			}

			//Уничтожение изображения
			imagedestroy($resized_img);

			$this->array_colors = $colors;
			return true;
		}else{
			return false;
		}

	}

	//функция - нахождение самого распространенного цвета
	private function get_main_color(){ 
		if (is_array($this->array_colors)) {
			//Подсчитываем количество повторений всех значений массива
			$arr_count_val_colors = array_count_values($this->array_colors);
			//сортируем по убыванию
			arsort($arr_count_val_colors);
			//получаем ключ первого элемента массива - ключь это значение цвета
			$this->result_color = array_key_first($arr_count_val_colors);
			return true;
		}else{
			return false;
		}
	}

	//функция - вывод результата
	public function out_color() {
		if($this->get_color_parts_img()){
			$this->get_main_color();
		    return '<div style="display:block; float:left; width:70px; height:20px; text-align:center; background:#'.$this->result_color.'">#'.$this->result_color.'</div>&nbsp;наиболее повторяющийся цвет в картинке ниже<br><img src="'.$this->url_img.'" / >';
		}else{
			return "Ошибка обработки входного изображения!";
		}
	}
}

// пример картинки
$url='https://alef.im/php-test-colors.jpg';
$color_img = new getMainColorInImage($url);
$res = $color_img->out_color();
echo $res;

?>